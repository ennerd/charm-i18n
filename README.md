Charm/I18n
==========

Given a ServerRequestInterface object, provides essential instances for internationalization; in particular the \Collator class
and the \Locale class.

Installation
------------

```
composer require charm/i18n
```

Usage
-----

```
// A Locale instance from the current request
$locale = Charm\I18n\Locale::fromRequest($serverRequest);

// The system default Locale instance
$locale = Charm\I18n\Locale::instance();

// A Collator instance from the current request
$collator = Charm\I18n\Collator::fromRequest($serverRequest);

// The system default Collator instance
$collator = Charm\I18n\Collator::instance();
```

