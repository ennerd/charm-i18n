<?php
declare(strict_types=1);

namespace Charm\I18n;

use Psr\Http\Message\ServerRequestInterface;

class Locale extends \Locale
{
    /**
     * Configuration variable for the locale. Uses system default locale as default.
     */
    public static ?string $locale = null;

    public static function getDefault(): string
    {
        if (static::$locale) {
            return static::$locale;
        }

        return parent::getDefault();
    }

    /**
     * Tries to find out best available locale based on HTTP "Accept-Language" header.
     */
    public static function acceptFromServerRequest(ServerRequestInterface $serverRequest): string
    {
        $acceptLanguageHeader = $serverRequest->getHeaderLine('Accept-Language');

        return static::acceptFromHttp($acceptLanguageHeader);
    }
}
