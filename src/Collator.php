<?php
declare(strict_types=1);

namespace Charm\I18n;

use Psr\Http\Message\ServerRequestInterface;

class Collator extends \Collator
{
    public static function instance(): self
    {
        return new self(Locale::getDefault());
    }

    public static function acceptFromServerRequest(ServerRequestInterface $serverRequest): self
    {
        return new self(Locale::acceptFromServerRequest($serverRequest));
    }
}
